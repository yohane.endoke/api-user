# Étape de build
FROM rust:1.72 as builder

WORKDIR /app

# Copier les fichiers sources dans le conteneur
COPY . .

# Compile l'application en mode release
RUN cargo build --release

# Étape de production
FROM debian:buster-slim

WORKDIR /usr/local/bin

# Installer les bibliothèques nécessaires
RUN apt-get update && apt-get install -y \
    libssl-dev \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# Copier le binaire compilé depuis l'étape de build
COPY --from=builder /app/target/release/customer-api .

# Définit le point d'entrée pour l'application
CMD ["./customer-api"]