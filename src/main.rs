use actix_web::{web, App, HttpServer, HttpResponse, Responder};
use serde::{Serialize, Deserialize};
use uuid::Uuid;
use std::env;
use jsonwebtoken::{encode, decode, Header, Algorithm, Validation, EncodingKey, DecodingKey};
use std::time::{SystemTime, UNIX_EPOCH};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use reqwest::Client;
use tokio_postgres::{NoTls, Error as PostgresError};
use dotenv::dotenv;

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    sub: String,
    exp: usize,
}

#[derive(Serialize, Deserialize)]
struct Customer {
    id: Uuid,
    username: String,
    firstname: String,
    lastname: String,
    created_at: String,
}

/*const OK_RESPONSE: &str = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n";
const NOT_FOUND_RESPONSE: &str = "HTTP/1.1 404 NOT FOUND\r\n\r\n";
const INTERNAL_SERVER_ERROR_RESPONSE: &str = "HTTP/1.1 500 INTERNAL SERVER ERROR\r\n\r\n";
*/

fn generate_jwt(username: &str) -> String {
    let secret_key = env::var("JWT_SECRET").expect("JWT_SECRET must be set");

    let expiration = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_secs() as usize
        + 3600;

    let claims = Claims {
        sub: username.to_owned(),
        exp: expiration,
    };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret_key.as_ref()),
    ).unwrap()
}

fn validate_jwt(token: &str) -> Result<Claims, jsonwebtoken::errors::Error> {
    let secret_key = env::var("JWT_SECRET").expect("JWT_SECRET must be set");

    decode::<Claims>(
        token,
        &DecodingKey::from_secret(secret_key.as_ref()),
        &Validation::new(Algorithm::HS256),
    ).map(|data| data.claims)
}

async fn handle_login(data: web::Json<Customer>) -> impl Responder {
    let token = generate_jwt(&data.username);
    HttpResponse::Ok().body(token)
}

async fn get_customers(auth: BearerAuth) -> impl Responder {
    if validate_jwt(auth.token()).is_err() {
        return HttpResponse::Unauthorized().body("Invalid token");
    }

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.expect("Error connecting to the database");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let mut customers = Vec::new();
    for row in client.query("SELECT * FROM customers", &[]).await.expect("Query failed") {
        customers.push(Customer {
            id: row.get("id"),
            username: row.get("username"),
            firstname: row.get("firstname"),
            lastname: row.get("lastname"),
            created_at: row.get("created_at"),
        });
    }

    HttpResponse::Ok().json(customers)
}

async fn get_customer_by_id(auth: BearerAuth, path: web::Path<Uuid>) -> impl Responder {
    if validate_jwt(auth.token()).is_err() {
        return HttpResponse::Unauthorized().body("Invalid token");
    }

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.expect("Error connecting to the database");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let id = path.into_inner();
    let row = client.query_one("SELECT * FROM customers WHERE id = $1", &[&id]).await;

    match row {
        Ok(row) => {
            let customer = Customer {
                id: row.get("id"),
                username: row.get("username"),
                firstname: row.get("firstname"),
                lastname: row.get("lastname"),
                created_at: row.get("created_at"),
            };
            HttpResponse::Ok().json(customer)
        },
        Err(_) => HttpResponse::NotFound().body("Customer not found"),
    }
}

async fn create_customer(auth: BearerAuth, data: web::Json<Customer>) -> impl Responder {
    if validate_jwt(auth.token()).is_err() {
        return HttpResponse::Unauthorized().body("Invalid token");
    }

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.expect("Error connecting to the database");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let customer = data.into_inner();
    let id = Uuid::new_v4();
    client.execute(
        "INSERT INTO customers (id, username, firstname, lastname, created_at) VALUES ($1, $2, $3, $4, $5)",
        &[&id, &customer.username, &customer.firstname, &customer.lastname, &customer.created_at],
    ).await.expect("Insert failed");

    HttpResponse::Ok().body("Customer created")
}

async fn update_customer(auth: BearerAuth, path: web::Path<Uuid>, data: web::Json<Customer>) -> impl Responder {
    if validate_jwt(auth.token()).is_err() {
        return HttpResponse::Unauthorized().body("Invalid token");
    }

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.expect("Error connecting to the database");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let id = path.into_inner();
    let customer = data.into_inner();

    client.execute(
        "UPDATE customers SET username = $1, firstname = $2, lastname = $3, created_at = $4 WHERE id = $5",
        &[&customer.username, &customer.firstname, &customer.lastname, &customer.created_at, &id],
    ).await.expect("Update failed");

    HttpResponse::Ok().body("Customer updated")
}

async fn delete_customer(auth: BearerAuth, path: web::Path<Uuid>) -> impl Responder {
    if validate_jwt(auth.token()).is_err() {
        return HttpResponse::Unauthorized().body("Invalid token");
    }

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await.expect("Error connecting to the database");
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let id = path.into_inner();
    let rows_affected = client.execute("DELETE FROM customers WHERE id = $1", &[&id]).await.expect("Delete failed");

    if rows_affected == 0 {
        return HttpResponse::NotFound().body("Customer not found");
    }

    HttpResponse::Ok().body("Customer deleted")
}
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    set_database().await.expect("Failed to set up database");

    let client = Client::new();
    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(client.clone()))
            .route("/login", web::post().to(handle_login))
            .route("/customers", web::get().to(get_customers))
            .route("/customers/{id}", web::get().to(get_customer_by_id))
            .route("/customers", web::post().to(create_customer))
            .route("/customers/{id}", web::put().to(update_customer))
            .route("/customers/{id}", web::delete().to(delete_customer))
    })
        .bind("0.0.0.0:8080")?
        .run()
        .await
}

async fn set_database() -> Result<(), PostgresError> {
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let (client, connection) = tokio_postgres::connect(&db_url, NoTls).await?;

    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    client.batch_execute("
        CREATE TABLE IF NOT EXISTS customers (
            id UUID PRIMARY KEY,
            username TEXT NOT NULL,
            firstname TEXT NOT NULL,
            lastname TEXT NOT NULL,
            created_at TEXT NOT NULL
        )
    ").await?;

    Ok(())
}
