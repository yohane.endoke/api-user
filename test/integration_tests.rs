#[cfg(test)]
mod integration_tests {
    use actix_web::{test, App};
    use super::*;
    use uuid::Uuid;

    #[actix_rt::test]
    async fn test_login() {
        let mut app = test::init_service(App::new().route("/login", web::post().to(handle_login))).await;
        let payload = Customer {
            id: Uuid::new_v4(),
            username: "testuser".to_string(),
            firstname: "Test".to_string(),
            lastname: "User".to_string(),
            created_at: "2024-06-26".to_string(),
        };
        let req = test::TestRequest::post().uri("/login").set_json(&payload).to_request();
        let resp: String = test::call_and_read_body_json(&mut app, req).await;
        assert!(resp.contains("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"));
    }

    #[actix_rt::test]
    async fn test_get_customers() {
        let mut app = test::init_service(App::new().route("/customers", web::get().to(get_customers))).await;
        let req = test::TestRequest::get()
            .uri("/customers")
            .header("Authorization", "Bearer YOUR_JWT_TOKEN_HERE")
            .to_request();
        let resp: Vec<Customer> = test::call_and_read_body_json(&mut app, req).await;
        assert_eq!(resp.len(), 1);
        assert_eq!(resp[0].username, "johndoe");
    }

    #[actix_rt::test]
    async fn test_get_customer_by_id() {
        let mut app = test::init_service(App::new().route("/customers/{id}", web::get().to(get_customer_by_id))).await;
        let customer_id = Uuid::new_v4();
        let req = test::TestRequest::get()
            .uri(&format!("/customers/{}", customer_id))
            .header("Authorization", "Bearer YOUR_JWT_TOKEN_HERE")
            .to_request();
        let resp: Customer = test::call_and_read_body_json(&mut app, req).await;
        assert_eq!(resp.id, customer_id);
    }

    #[actix_rt::test]
    async fn test_create_customer() {
        let mut app = test::init_service(App::new().route("/customers", web::post().to(create_customer))).await;
        let payload = Customer {
            id: Uuid::new_v4(),
            username: "newuser".to_string(),
            firstname: "New".to_string(),
            lastname: "User".to_string(),
            created_at: "2024-06-26".to_string(),
        };
        let req = test::TestRequest::post()
            .uri("/customers")
            .header("Authorization", "Bearer YOUR_JWT_TOKEN_HERE")
            .set_json(&payload)
            .to_request();
        let resp = test::call_and_read_body(&mut app, req).await;
        assert_eq!(resp, "Customer created");
    }

    #[actix_rt::test]
    async fn test_update_customer() {
        let mut app = test::init_service(App::new().route("/customers/{id}", web::put().to(update_customer))).await;
        let customer_id = Uuid::new_v4();
        let payload = Customer {
            id: customer_id,
            username: "updateduser".to_string(),
            firstname: "Updated".to_string(),
            lastname: "User".to_string(),
            created_at: "2024-06-26".to_string(),
        };
        let req = test::TestRequest::put()
            .uri(&format!("/customers/{}", customer_id))
            .header("Authorization", "Bearer YOUR_JWT_TOKEN_HERE")
            .set_json(&payload)
            .to_request();
        let resp = test::call_and_read_body(&mut app, req).await;
        assert_eq!(resp, "Customer updated");
    }

    #[actix_rt::test]
    async fn test_delete_customer() {
        let mut app = test::init_service(App::new().route("/customers/{id}", web::delete().to(delete_customer))).await;
        let customer_id = Uuid::new_v4();
        let req = test::TestRequest::delete()
            .uri(&format!("/customers/{}", customer_id))
            .header("Authorization", "Bearer YOUR_JWT_TOKEN_HERE")
            .to_request();
        let resp = test::call_and_read_body(&mut app, req).await;
        assert_eq!(resp, "Customer deleted");
    }
}
