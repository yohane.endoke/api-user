#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App};
    use actix_web::http::StatusCode;
    use serde_json::json;

    #[actix_rt::test]
    async fn test_login() {
        let customer = Customer {
            id: Uuid::new_v4(),
            username: "testuser".to_string(),
            firstname: "Test".to_string(),
            lastname: "User".to_string(),
            created_at: "2022-01-01".to_string(),
        };

        let mut app = test::init_service(
            App::new()
                .route("/login", web::post().to(handle_login))
        ).await;

        let req = test::TestRequest::post()
            .uri("/login")
            .set_json(&customer)
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn test_get_customers_unauthorized() {
        let mut app = test::init_service(
            App::new()
                .route("/customers", web::get().to(get_customers))
        ).await;

        let req = test::TestRequest::get()
            .uri("/customers")
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    // Add more tests for each endpoint
}
